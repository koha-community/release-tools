# Koha release tools

The release tools repository contains a collection of scripts to aid release mantainers and release managers in their
tasks during their tenure in their respective roles.

## Requirements

### Software

The project has recently been dockerized to simplify it's use, all you need is:

- A text editor to tweak configuration files
- Docker ([install instructions](https://docs.docker.com/engine/install/))

### Setup

It is not a bad idea to organize your projects on a directory. For the purpose of simplifying the instructions we will pick `~/git` as the place in which to put all the repository clones:

```shell
mkdir -p ~/git
export PROJECTS_DIR=~/git
```

- Clone the release-tools repository

```shell
cd $PROJECTS_DIR
git clone https://gitlab.com/koha-community/release-tools.git
```

- Clone the koha-manual repository

```shell
cd $PROJECTS_DIR
git clone https://gitlab.com/koha-community/koha-manual.git
```

- Copy the example configuration file and use your favourite editor to update it to reflect your own details

```shell
cp etc/user.example etc/user.yaml
vim etc/user.yaml
```

- Set some *mandatory* environment variables:

```shell
export RELEASE_TOOLS_DIR=$PROJECTS_DIR/release-tools
echo "export SYNC_REPO=$PROJECTS_DIR/koha" >> ~/.bashrc
echo "export RELEASE_TOOLS_USER_CONFIG=$RELEASE_TOOLS_DIR/etc/user.yaml" >> ~/.bashrc
echo "export KOHA_MANUAL_REPO=$PROJECTS_DIR/koha-manual" >> ~/.bashrc
echo "export PATH=\$PATH:$RELEASE_TOOLS_DIR" >> ~/.bashrc
```

## Usage

After that setup, you should be able to run the container like this:

```shell
release-tools --shell
```

*Note:* If the above doesn't work yet, it's likely you need to re-source your shell:

```shell
source ~/.bashrc
```

## Always pull the latest release tools image!

**IMPORTANT**: it is recommended that you keep your local `release-tools` image up to date.
Always check with the rest of the Release Team if you have any doubts about this.

In order to pull the latest image, you can manually run:

```shell
release-tools --pull
```

### Scripts

The most commonly used scripts can be found within the bin/ directory, but they are generally written to be run from
within your koha source clone repository.

```shell
cd /koha
```

#### bin/koha-push

Used to check for new authors and update bugzilla prior to pushing your branch.

The script should be run from within your koha source directory.

```shell
koha-push updatebz
koha-push updatebz --range HASH..HEAD
koha-push updatebz --range HASH..HEAD
```

Upon execution it will walk through all commits that appear in your local branch
but do not yet appear upstream.
It will update etc/sponsor_map.yaml (from release_tools repo) and docs/contributors.yaml
(from Koha repo).
The script will then mark the relevant bugs as 'Pushed to X', add the relevant
version to the versions released box and add your configured thankyou comment to the bug.

#### bin/koha-release

Used to build the release notes and report release information.

The script should be called from within your koha source directory.

```shell
koha-release v19.11.00..HEAD info
koha-release v19.11.00..HEAD notes
koha-release v19.11.00..HEAD notes --silent
koha-release v19.11.00..HEAD html misc/release_notes/release_notes_19_11_01.html
koha-release v19.11.00..HEAD tarball --branch BRANCH
```

Upon execution it will walk through all commits in your given range and generate
a set of release notes from their content sourcing data from the commits, 
bugzilla and the translation site.  It will interactively ask you about author 
details should it find a commit with an unrecognised author, unless the --silent
parameter is passed. 

* The `notes` command will output a markdown file.
* The `html` command will read in the aforementioned markdown file and general an html file.
* The `tarball` command will produce the signed tar archives to be uploaded to download.koha-community.org

![Koha Logo](https://wiki.koha-community.org/w/images/KohaILS.png)
