FROM debian:stable-slim

# File Author / Maintainer
LABEL maintainer="tomascohen@theke.io"

ENV PATH /usr/bin:/bin:/usr/sbin:/sbin
ENV DEBIAN_FRONTEND noninteractive

RUN mkdir /release-tools
COPY . /release-tools

# Set suitable debian sources
RUN echo "deb http://httpredir.debian.org/debian buster main" > /etc/apt/sources.list
RUN echo "deb http://security.debian.org/ buster/updates main" >> /etc/apt/sources.list

ENV REFRESHED_AT 1970-01-01

RUN apt-get -y update \
    && apt-get -y install \
      bugz \
      cpanminus \
      git \
      gnupg \
      locales \
      make \
      tar \
      libencode-perl \
      libfile-slurp-perl \
      libhtml-tableextract-perl \
      libio-prompt-tiny-perl \
      libjson-perl \
      liblingua-en-numbers-ordinate-perl \
      liblist-moreutils-perl \
      libmodern-perl-perl \
      libmoose-perl \
      librest-client-perl \
      libtemplate-perl \
      libtext-multimarkdown-perl \
      libtime-moment-perl \
      liburi-perl \
      libutf8-all-perl \
      libwww-perl \
      libwww-mechanize-perl \
      libyaml-perl \
      perl-doc \
   && rm -rf /var/cache/apt/archives/* \
   && rm -rf /var/lib/apt/lists/* \
   && cd /release-tools && cpanm --installdeps . \
   && rm -r /root/.cpanm

# Set locales
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen \
    && dpkg-reconfigure locales \
    && /usr/sbin/update-locale LANG=en_US.UTF-8

RUN    echo "export PATH=${PATH}:/release-tools/bin" > /root/.bashrc \
    && echo "export PERL5LIB=/koha:/release-tools/lib" >> /root/.bashrc

RUN git config --global --add safe.directory /koha

ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8
